var gulp = require('gulp');
var logger = require('gulp-logger');
var less = require('gulp-less');
var path = require('path');
var minifyCSS = require('gulp-minify-css');
var rename = require('gulp-rename');

/* Task to compile less */
gulp.task('compile-less', function() {
    gulp.src('public_html/assets/less/christo/styles.less')
        .pipe(less())
        .pipe(gulp.dest('public_html/assets/css/'));
});

/* Task to watch less changes */
gulp.task('watch-less', function() {
    gulp.watch('./assets/src/**/*.less' , ['compile-less']);
});

/* Task to minify css */
gulp.task('minify-css', function() {
    gulp.src('public_html/assets/css/styles.css')
        .pipe(minifyCSS())
        .pipe(rename('styles.min.css'))
        .pipe(gulp.dest( 'public_html/assets/css/' ));
});

/* Task when running `gulp` from terminal */
gulp.task('default', ['compile-less', 'watch-less']);

/* Task when running `gulp build` from terminal */
gulp.task('build', ['compile-less', 'minify-css']);
